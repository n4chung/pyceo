import os
import socket
import sys

from zope import component

from .cli import cli
from .krb_check import krb_check
from .tui.start import main as tui_main
from ceo_common.interfaces import IConfig, IHTTPClient
from ceo_common.model import Config, HTTPClient


def register_services():
    # Using base component directly so events get triggered
    baseComponent = component.getGlobalSiteManager()

    # Config
    if 'CEO_CONFIG' in os.environ:
        config_file = os.environ['CEO_CONFIG']
    else:
        # This is a hack to determine if we're in the dev env or not
        if socket.getfqdn().endswith('.csclub.internal'):
            config_file = './tests/ceo_dev.ini'
        else:
            config_file = '/etc/csc/ceo.ini'
    cfg = Config(config_file)
    baseComponent.registerUtility(cfg, IConfig)

    # HTTPService
    http_client = HTTPClient()
    baseComponent.registerUtility(http_client, IHTTPClient)


def main():
    krb_check()
    register_services()
    if len(sys.argv) > 1:
        cli(obj={})
    else:
        tui_main()


if __name__ == '__main__':
    main()
