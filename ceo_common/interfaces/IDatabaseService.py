from zope.interface import Attribute, Interface


class IDatabaseService(Interface):
    """Interface to create databases for users."""

    type = Attribute('the type of databases that will be created')
    auth_username = Attribute('username to a privileged user on the database host')
    auth_password = Attribute('password to a privileged user on the database host')

    def create_db(username: str) -> str:
        """create a user and database and return the password"""

    def reset_passwd(username: str) -> str:
        """reset user password and return it"""

    def delete_db(username: str):
        """remove user and delete their database"""
