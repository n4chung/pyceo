from abc import ABC

import ceo.tui.utils as utils


# NOTE: one controller can control multiple views,
# but each view must have exactly one controller
class Controller(ABC):
    class InvalidInput(Exception):
        pass

    class RequestFailed(Exception):
        pass

    def __init__(self, model, app):
        super().__init__()
        self.model = model
        self.app = app
        # Since the view and the controller both have a reference to each
        # other, this needs to be initialized in a separate step
        self.view = None

    def _push_history(self, old_view, new_view):
        if new_view.model.name == 'Welcome':
            self.app.history.clear()
        else:
            self.app.history.append(old_view)

    def switch_to_view(self, new_view):
        self._push_history(self.view, new_view)
        self.view = new_view
        new_view.activate()

    def go_to_next_menu(self, next_menu_name):
        _, new_view, _ = utils.get_mvc(self.app, next_menu_name)
        self._push_history(self.view, new_view)
        new_view.activate()

    def prev_menu_callback(self, button):
        prev_view = self.app.history.pop()
        prev_view.controller.view = prev_view
        prev_view.activate()

    def next_menu_callback(self, button, next_menu_name):
        self.go_to_next_menu(next_menu_name)

    def get_next_menu_callback(self, next_menu_name):
        def callback(button):
            self.next_menu_callback(button, next_menu_name)
        return callback

    def get_username_from_view(self):
        username = self.view.username_edit.edit_text
        # TODO: share validation logic between CLI and TUI
        if not username:
            self.view.popup('Username must not be empty')
            raise Controller.InvalidInput()
        return username

    def get_group_name_from_view(self):
        name = self.view.name_edit.edit_text
        # TODO: share validation logic between CLI and TUI
        if not name:
            self.view.popup('Name must not be empty')
            raise Controller.InvalidInput()
        return name

    def get_num_terms_from_view(self):
        num_terms_str = self.view.num_terms_edit.edit_text
        if num_terms_str:
            num_terms = int(num_terms_str)
        else:
            num_terms = 0
        # TODO: share validation logic between CLI and TUI
        if num_terms <= 0:
            self.view.popup('Number of terms must be a positive integer')
            raise Controller.InvalidInput()
        return num_terms
