# Packaging

This is a guide for creating Debian packages for ceo. The instructions below
probably do not follow best practices, but at least I am confident that they
work.

## Prerequisites
Make sure your GPG key is in /srv/debian/gpg on potassium-benzoate. See
[here](https://wiki.csclub.uwaterloo.ca/Debian_Repository#Step_1:_Add_to_Uploaders)
for instructions.

Make sure you are in the `csc-mirror` group too.

## Creating the package
Use Docker/Podman to avoid screwing up your main system.
For example, to create a package for bullseye (replace `podman` with `docker` in all instances below if you're using Docker):
```sh
podman run -it --name pyceo-packaging -v "$PWD":"$PWD" -w "$PWD" debian:bullseye bash
```
**Important**: Make sure to use a container image for the same distribution which you're packaging.
For example, if you're creating a package for bullseye, you should be using the debian:bullseye
Docker image (this is because the virtualenv symlinks python to the OS' version of python).

Here are some of the prerequisites you'll need to build the deb files
(run this inside the container):
```sh
apt update
apt install -y devscripts debhelper git-buildpackage vim
```
Make sure to also install all of the packages in the 'Build-Depends' section in debian/control.

Update VERSION.txt to the next version, and do a git commit.

Now run `dch -i` and edit the changelog.

Now you will build a signed package. Place your key ID after the `-k` argument, e.g.
```sh
gbp buildpackage --git-upstream-branch=master -k8E5568ABB0CF96BC367806ED127923BE10DA48DC
```
This will create a bunch of files (deb, dsc, tar.gz, etc.) in the parent directory.

Now do another git commit (since you edited the changelog file).

To clean the packages:
```sh
rm ../*.{xz,gz,dsc,build,buildinfo,changes,deb}
```

## Uploading the package
Inside the container, go up one directory, and create a tarball with all the package files:
```
cd ..
tar zcvf pyceo.tar.gz *.{xz,gz,dsc,build,buildinfo,changes,deb}
```
Outside of the container (i.e. on your personal machine), copy the tarball out of the
container into your current directory, e.g.
```
podman cp pyceo-packaging:/home/max/repos/pyceo.tar.gz .
```
(Replace `/home/max/repos` by the directory in the container with the tarball.)
Now upload the tarball to a CSC machine, e.g.
```
scp pyceo.tar.gz mannitol:~
```
SSH into that machine and extract the tarball into a separate directory:
```
ssh mannitol
mkdir pyceo-parent
mv pyceo.tar.gz pyceo-parent/
cd pyceo-parent
tar zxvf pyceo.tar.gz
```
At this point, you will need a dupload.conf file. Ask someone on syscom for a copy.

Now upload the package to potassium-benzoate:
```
kinit
dupload *.changes
```

Now SSH into potassium-benzoate and run the following:
```
sudo /srv/debian/bin/rrr-incoming
```

There, that wasn't so bad...right? :')
