#!/usr/bin/make -f
SCDFILES = $(wildcard docs/*.scd)
MANPAGES = $(patsubst docs/%.scd,docs/%,${SCDFILES})
CEO_HOME = /var/lib/ceo

build: docs venv

venv:
	python3 -m venv venv && \
	  . venv/bin/activate && \
	  pip install --upgrade pip && \
	  pip install setuptools wheel && \
	  pip install -r requirements.txt && \
	  pip install .

install:
	@# Prepare the virtualenv to be moved (dangerous!)
	@# Make sure you don't have '|' in your paths
	grep -IRl $(CURDIR)/venv venv/bin | \
	  xargs perl -pe 's|\Q$(CURDIR)/venv\E|$(CEO_HOME)/venv|g' -i
	mkdir -p $(DESTDIR)$(CEO_HOME)
	mv venv $(DESTDIR)$(CEO_HOME)

docs:
	for file in ${SCDFILES} ; do \
	  scdoc < $$file > `echo $$file | grep -oP '.*(?=\.scd$$)'` ; \
	done

clean:
	rm -f ${MANPAGES}
	rm -rf venv
	rm -rf debian/{ceo,ceod,ceo-common,tmp}

.PHONY: build docs clean venv install
