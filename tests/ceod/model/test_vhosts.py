import json
import os
import pytest


def test_vhost_mgr(vhost_mgr):
    rate_limit_file = '/run/ceod/rate_limit.json'
    if os.path.exists(rate_limit_file):
        os.unlink(rate_limit_file)
    username = 'test1'
    domain = username + '.csclub.cloud'
    filename = f'{username}_{domain}'
    ip_address = '172.19.134.11'
    vhost_mgr.create_vhost(username, domain, ip_address)
    path = os.path.join(vhost_mgr.vhost_dir, filename)
    assert os.path.isfile(path)

    assert vhost_mgr.get_num_vhosts(username) == 1

    assert vhost_mgr.get_vhosts(username) == [{
        'domain': domain, 'ip_address': ip_address,
    }]

    d = json.load(open(rate_limit_file))
    assert username in d['create_vhost']
    os.unlink(rate_limit_file)

    domain2 = 'app.' + domain
    vhost_mgr.create_vhost(username, domain2, ip_address)
    assert vhost_mgr.get_num_vhosts(username) == 2

    vhost_mgr.delete_vhost(username, domain)
    assert vhost_mgr.get_num_vhosts(username) == 1

    vhost_mgr.delete_all_vhosts_for_user(username)
    assert vhost_mgr.get_num_vhosts(username) == 0

    os.unlink(rate_limit_file)
    vhost_mgr.create_vhost(username, domain, ip_address + ':8000')
    os.unlink(rate_limit_file)
    domain3 = username + '.k8s.csclub.cloud'
    vhost_mgr.create_vhost(username, domain3, 'k8s')
    assert vhost_mgr.get_vhosts(username) == [
        {'domain': domain, 'ip_address': ip_address + ':8000'},
        {'domain': domain3, 'ip_address': 'k8s'},
    ]

    vhost_mgr.delete_all_vhosts_for_user(username)
    os.unlink(rate_limit_file)


@pytest.mark.parametrize('suffix', ['csclub.cloud', 'k8s.csclub.cloud'])
@pytest.mark.parametrize('prefix,is_valid', [
    ('ctdalek', True),
    ('ctdalek1', False),
    ('1ctdalek', False),
    ('app_ctdalek', False),
    ('app.ctdalek', True),
    ('ctdalek.app', False),
    ('app-ctdalek', True),
    ('ctdalek-app', False),
    ('abc.def.ctdalek', True),
    ('abc.def-ctdalek', True),
])
def test_vhost_domain_validation(suffix, prefix, is_valid, vhost_mgr):
    username = 'ctdalek'
    domain = prefix + '.' + suffix
    assert vhost_mgr.is_valid_domain(username, domain) == is_valid


def test_vhost_domain_validation_2(vhost_mgr):
    assert not vhost_mgr.is_valid_domain('ctdalek', 'ctdalek.csclub.internal')


@pytest.mark.parametrize('ip_address,is_valid', [
    ('8.8.8.8', False),
    ('172.19.134.11', True),
    ('172.19.134.11:8000', True),
    ('172.19.134.1', False),
    ('172.19.134.254', False),
    ('172.19.134.254:8000', False),
    ('k8s', True),
])
def test_vhost_ip_validation(ip_address, is_valid, vhost_mgr):
    assert vhost_mgr.is_valid_ip_address(ip_address) == is_valid
