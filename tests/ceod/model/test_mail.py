def test_welcome_message(cfg, mock_mail_server, mail_srv, simple_user):
    base_domain = cfg.get('base_domain')
    mock_mail_server.messages.clear()
    mail_srv.send_welcome_message_to(simple_user, 'password')
    msg = mock_mail_server.messages[0]
    assert msg['from'] == f'exec@{base_domain}'
    assert msg['to'] == f'{simple_user.uid}@{base_domain}'
    # make sure that templating was applied correctly
    first_name = simple_user.cn.split()[0]
    assert f'Hello {first_name}' in msg['content']
    mock_mail_server.messages.clear()
