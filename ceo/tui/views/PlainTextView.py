import urwid

from .View import View
from .utils import wrap_in_frame
from ceo.tui.app import App


class PlainTextView(View):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)

    def set_lines(
        self,
        lines,
        align='center',
        scrollable=False,
        min_width=None,
        on_back=None,
        on_next=None,
        flash_text=None,
        linebox=False,
        subtitle=None,
    ):
        if min_width is None:
            if align == 'center':
                min_width = App.WIDTH
            else:
                min_width = max(map(len, lines))
        if scrollable:
            body = urwid.ListBox(urwid.SimpleListWalker([
                urwid.Text(line, align=align)
                for line in lines
            ]))
        else:
            body = urwid.Filler(
                urwid.Text('\n'.join(lines), align=align)
            )
        if linebox:
            body = urwid.LineBox(body)
        self.original_widget = wrap_in_frame(
            urwid.Padding(
                body,
                align='center',
                width=('relative', App.REL_WIDTH_PCT),
                min_width=min_width,
            ),
            self.model.title,
            subtitle=subtitle,
            on_back=on_back,
            on_next=on_next,
            flash_text=flash_text,
        )
