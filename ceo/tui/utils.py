import json

from ceo.tui.controllers import *
from ceo.tui.models import *
from ceo.tui.views import *


def handle_sync_response(resp, controller):
    if resp.ok:
        if resp.headers.get('content-type') == 'application/json':
            return resp.json()
        # streaming response
        return [json.loads(line) for line in resp.text.splitlines()]

    def target():
        view = ErrorView(controller.model, controller, controller.app)
        controller.switch_to_view(view)

    if resp.headers.get('content-type') == 'application/json':
        err_msg = resp.json()['error']
    else:
        err_msg = resp.text.rstrip()
    controller.model.error_message = err_msg
    controller.app.run_in_main_loop(target)
    raise Controller.RequestFailed()


def get_mvc(app, name):
    if name == WelcomeModel.name:
        model = WelcomeModel()
        controller = WelcomeController(model, app)
        view = WelcomeView(model, controller, app)
    elif name == AddUserModel.name:
        model = AddUserModel()
        controller = AddUserController(model, app)
        view = AddUserView(model, controller, app)
    elif name == RenewUserModel.name:
        model = RenewUserModel()
        controller = RenewUserController(model, app)
        view = RenewUserView(model, controller, app)
    elif name == GetUserModel.name:
        model = GetUserModel()
        controller = GetUserController(model, app)
        view = GetUserView(model, controller, app)
    elif name == ResetPasswordModel.name:
        model = ResetPasswordModel()
        controller = ResetPasswordController(model, app)
        view = ResetPasswordView(model, controller, app)
    elif name == ChangeLoginShellModel.name:
        model = ChangeLoginShellModel()
        controller = ChangeLoginShellController(model, app)
        view = ChangeLoginShellView(model, controller, app)
    elif name == AddGroupModel.name:
        model = AddGroupModel()
        controller = AddGroupController(model, app)
        view = AddGroupView(model, controller, app)
    elif name == GetGroupModel.name:
        model = GetGroupModel()
        controller = GetGroupController(model, app)
        view = GetGroupView(model, controller, app)
    elif name == AddMemberToGroupModel.name:
        model = AddMemberToGroupModel()
        controller = AddMemberToGroupController(model, app)
        view = AddMemberToGroupView(model, controller, app)
    elif name == RemoveMemberFromGroupModel.name:
        model = RemoveMemberFromGroupModel()
        controller = RemoveMemberFromGroupController(model, app)
        view = RemoveMemberFromGroupView(model, controller, app)
    elif name == CreateDatabaseModel.name:
        model = CreateDatabaseModel()
        controller = CreateDatabaseController(model, app)
        view = CreateDatabaseView(model, controller, app)
    elif name == ResetDatabasePasswordModel.name:
        model = ResetDatabasePasswordModel()
        controller = ResetDatabasePasswordController(model, app)
        view = ResetDatabasePasswordView(model, controller, app)
    elif name == GetPositionsModel.name:
        model = GetPositionsModel()
        controller = GetPositionsController(model, app)
        view = GetPositionsView(model, controller, app)
    elif name == SetPositionsModel.name:
        model = SetPositionsModel()
        controller = SetPositionsController(model, app)
        view = SetPositionsView(model, controller, app)
    else:
        raise NotImplementedError()
    controller.view = view
    return model, view, controller
