import click

from .database import create as db_create, pwreset as db_pwreset, delete as db_delete


@click.group(short_help='Perform operations on PostgreSQL')
def postgresql():
    pass


@postgresql.command(short_help='Create a PostgreSQL database for a user')
@click.argument('username')
def create(username):
    db_create(username, 'postgresql')


@postgresql.command(short_help='Reset the password of a PostgreSQL user')
@click.argument('username')
def pwreset(username):
    db_pwreset(username, 'postgresql')


@postgresql.command(short_help="Delete the database of a PostgreSQL user")
@click.argument('username')
def delete(username):
    db_delete(username, 'postgresql')
