from zope.interface import Interface


class IConfig(Interface):
    """Represents a config store."""

    def get(key: str) -> str:
        """Get the config value for the given key."""
