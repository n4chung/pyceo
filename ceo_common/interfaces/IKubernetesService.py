from typing import List

from zope.interface import Interface


class IKubernetesService(Interface):
    """A client for the syscom-managed k8s cluster on CloudStack."""

    def create_account(username: str) -> str:
        """
        Create a new k8s namespace for the given user and create new
        k8s credentials for them.
        The contents of a kubeconfig file are returned.
        """

    def delete_account(username: str) -> str:
        """
        Delete the k8s namespace for the given user.
        """

    def get_accounts() -> List[str]:
        """
        Get a list of users who have k8s namespaces.
        """
