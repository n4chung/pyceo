from flask import Blueprint, request
from zope import component

from .utils import authz_restrict_to_syscom, is_truthy, \
    create_streaming_response, development_only
from ceo_common.interfaces import ILDAPService
from ceod.transactions.groups import (
    AddGroupTransaction,
    AddMemberToGroupTransaction,
    RemoveMemberFromGroupTransaction,
    DeleteGroupTransaction,
)

bp = Blueprint('groups', __name__)


@bp.route('/', methods=['POST'], strict_slashes=False)
@authz_restrict_to_syscom
def create_group():
    body = request.get_json(force=True)
    txn = AddGroupTransaction(
        cn=body['cn'],
        description=body['description'],
    )
    return create_streaming_response(txn)


@bp.route('/<group_name>')
def get_group(group_name):
    ldap_srv = component.getUtility(ILDAPService)
    group = ldap_srv.get_group(group_name)
    return group.to_dict()


@bp.route('/<group_name>/members/<username>', methods=['POST'])
@authz_restrict_to_syscom
def add_member_to_group(group_name, username):
    subscribe_to_lists = is_truthy(
        request.args.get('subscribe_to_lists', 'true')
    )
    txn = AddMemberToGroupTransaction(
        username=username,
        group_name=group_name,
        subscribe_to_lists=subscribe_to_lists,
    )
    return create_streaming_response(txn)


@bp.route('/<group_name>/members/<username>', methods=['DELETE'])
@authz_restrict_to_syscom
def remove_member_from_group(group_name, username):
    unsubscribe_from_lists = is_truthy(
        request.args.get('unsubscribe_from_lists', 'true')
    )
    txn = RemoveMemberFromGroupTransaction(
        username=username,
        group_name=group_name,
        unsubscribe_from_lists=unsubscribe_from_lists,
    )
    return create_streaming_response(txn)


@bp.route('/<group_name>', methods=['DELETE'])
@authz_restrict_to_syscom
@development_only
def delete_group(group_name):
    txn = DeleteGroupTransaction(group_name)
    return create_streaming_response(txn)
