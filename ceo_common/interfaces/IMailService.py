from typing import Dict, List

from zope.interface import Interface

from .IUser import IUser


class IMailService(Interface):
    """An interface to send email messages."""

    def send(_from: str, to: str, headers: Dict[str, str], content: str):
        """Send a message with the given headers and content."""

    def send_welcome_message_to(user: IUser, password: str):
        """
        Send a welcome message to the new member, including their temporary
        password.
        """

    def announce_new_user(user: IUser, operations: List[str]):
        """
        Announce to the ceo mailing list that the new user was created.
        `operations` is a list of the operations which were performed
        during the transaction.
        """

    def send_membership_renewal_reminder(user: IUser):
        """
        Send a reminder to the user that their membership will expire
        soon.
        """

    def send_cloud_account_will_be_deleted_message(user: IUser):
        """
        Send a warning message to the user that their cloud resources
        will be deleted if they do not renew their membership.
        """

    def send_cloud_account_has_been_deleted_message(user: IUser):
        """
        Send a message to the user that their cloud resources have
        been deleted.
        """

    def send_club_website_has_been_disabled_message(club: str, address: str):
        """
        Send a message to the club stating that their website has been
        disabled.
        """
