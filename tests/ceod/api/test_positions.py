from ceod.model import User, Group


def test_get_positions(client, ldap_user, g_admin_ctx):
    with g_admin_ctx():
        ldap_user.set_positions(['president', 'treasurer'])
    status, data = client.get('/api/positions')
    assert status == 200
    expected = {
        'president': ldap_user.uid,
        'treasurer': ldap_user.uid,
    }
    assert data == expected


def test_set_positions(cfg, client, g_admin_ctx, mock_mailman_server):
    mock_mailman_server.clear()
    mailing_lists = cfg.get('auxiliary mailing lists_exec')
    base_domain = cfg.get('base_domain')

    users = []
    with g_admin_ctx():
        for uid in ['test_1', 'test_2', 'test_3', 'test_4']:
            user = User(uid=uid, cn='Some Name', given_name='Some', sn='Name',
                        terms=['s2021'])
            user.add_to_ldap()
            users.append(user)
        exec_group = Group(cn='exec', gid_number=10013)
        exec_group.add_to_ldap()

    try:
        # missing required position
        status, _ = client.post('/api/positions', json={
            'vice-president': 'test_1',
        })
        assert status == 400

        # non-existent position
        status, _ = client.post('/api/positions', json={
            'president': 'test_1',
            'vice-president': 'test_2',
            'sysadmin': 'test_3',
            'no-such-position': 'test_3',
        })
        assert status == 400

        status, data = client.post('/api/positions', json={
            'president': 'test_1',
            'vice-president': 'test_2',
            'sysadmin': 'test_3',
        })
        assert status == 200
        expected = [
            {"status": "in progress", "operation": "update_positions_ldap"},
            {"status": "in progress", "operation": "update_exec_group_ldap"},
            {"status": "in progress", "operation": "subscribe_to_mailing_lists"},
            {"status": "completed", "result": {
                "president": "test_1",
                "vice-president": "test_2",
                "sysadmin": "test_3",
            }},
        ]
        assert data == expected
        # make sure execs were added to exec group
        status, data = client.get('/api/groups/exec')
        assert status == 200
        expected = ['test_1', 'test_2', 'test_3']
        assert sorted([item['uid'] for item in data['members']]) == expected
        # make sure execs were subscribed to mailing lists
        addresses = [f'{uid}@{base_domain}' for uid in expected]
        for mailing_list in mailing_lists:
            assert sorted(mock_mailman_server.subscriptions[mailing_list]) == addresses

        _, data = client.post('/api/positions', json={
            'president': 'test_1',
            'vice-president': 'test_2',
            'sysadmin': 'test_2',
            'treasurer': 'test_4',
        })
        assert data[-1]['status'] == 'completed'
        # make sure old exec was removed from group
        expected = ['test_1', 'test_2', 'test_4']
        _, data = client.get('/api/groups/exec')
        assert sorted([item['uid'] for item in data['members']]) == expected
        # make sure old exec was removed from mailing lists
        addresses = [f'{uid}@{base_domain}' for uid in expected]
        for mailing_list in mailing_lists:
            assert sorted(mock_mailman_server.subscriptions[mailing_list]) == addresses
    finally:
        with g_admin_ctx():
            for user in users:
                user.remove_from_ldap()
            exec_group.remove_from_ldap()
        mock_mailman_server.clear()
