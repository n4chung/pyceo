import os

from click.testing import CliRunner

from ...utils import gssapi_token_ctx
from ceo.cli import cli


def test_cloud_account_activate(cli_setup, mock_cloud_server, new_user, cfg):
    base_domain = cfg.get('base_domain')
    mock_cloud_server.clear()

    runner = CliRunner()
    with gssapi_token_ctx(new_user.uid):
        result = runner.invoke(cli, ['cloud', 'account', 'activate'])
    expected = (
        'Congratulations! Your cloud account has been activated.\n'
        f'You may now login into https://cloud.{base_domain} with your CSC credentials.\n'
        "Make sure to enter 'Members' for the domain (no quotes).\n"
    )
    assert result.exit_code == 0
    assert result.output == expected


def test_cloud_accounts_purge(cli_setup, mock_cloud_server, mock_harbor_server):
    mock_cloud_server.clear()

    runner = CliRunner()
    result = runner.invoke(cli, ['cloud', 'accounts', 'purge'])
    assert result.exit_code == 0


def test_cloud_vhosts(cli_setup, new_user, cfg):
    members_domain = cfg.get('cloud vhosts_members_domain')
    uid = new_user.uid
    domain1 = uid + '.' + members_domain
    ip1 = '172.19.134.11'

    runner = CliRunner()
    with gssapi_token_ctx(uid):
        result = runner.invoke(cli, ['cloud', 'vhosts', 'add', domain1, ip1])
    expected = (
        'Please wait, this may take a while...\n'
        'Done.\n'
    )
    assert result.exit_code == 0
    assert result.output == expected

    with gssapi_token_ctx(uid):
        result = runner.invoke(cli, ['cloud', 'vhosts', 'list'])
    expected = domain1 + ': ' + ip1 + '\n'
    assert result.exit_code == 0
    assert result.output == expected

    with gssapi_token_ctx(uid):
        result = runner.invoke(cli, ['cloud', 'vhosts', 'delete', domain1])
    expected = 'Done.\n'
    assert result.exit_code == 0
    assert result.output == expected


def test_k8s_account_activate(cli_setup, new_user):
    uid = new_user.uid
    runner = CliRunner()
    old_home = os.environ['HOME']
    os.environ['HOME'] = new_user.home_directory
    try:
        with gssapi_token_ctx(uid):
            result = runner.invoke(cli, ['k8s', 'account', 'activate'])
    finally:
        os.environ['HOME'] = old_home
    expected = (
        "Congratulations! You have a new kubeconfig in ~/.kube/config.\n"
        "Run `kubectl cluster-info` to make sure everything is working.\n"
    )
    assert result.exit_code == 0
    assert result.output == expected
    assert os.path.isfile(os.path.join(new_user.home_directory, '.kube', 'config'))


def test_registry_project_create(cli_setup, mock_harbor_server, new_user):
    uid = new_user.uid
    runner = CliRunner()
    mock_harbor_server.reset()
    mock_harbor_server.users.append(uid)
    with gssapi_token_ctx(uid):
        result = runner.invoke(cli, ['registry', 'project', 'create'])
    expected = 'Congratulations! Your registry project was successfully created.\n'
    assert result.exit_code == 0
    assert result.output == expected
