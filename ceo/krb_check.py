import subprocess

import gssapi


_username = None


def get_username():
    """Get the user currently logged into CEO."""
    return _username


def krb_check():
    """
    Spawns a `kinit` process if no credentials are available or the
    credentials have expired.
    Stores the username for later use by get_username().
    """
    global _username
    for _ in range(2):
        try:
            creds = gssapi.Credentials(usage='initiate')
            result = creds.inquire()
            princ = str(result.name)
            _username = princ[:princ.index('@')]
            return
        except (gssapi.raw.misc.GSSError, gssapi.raw.exceptions.ExpiredCredentialsError):
            kinit()

    raise Exception('could not acquire GSSAPI credentials')


def kinit():
    subprocess.run(['kinit'], check=True)
