import re

from click.testing import CliRunner

from ceo.cli import cli


def test_groups(cli_setup, ldap_user):
    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'add', 'test_group_1', '-d', 'Test Group One',
    ], input='y\n')
    expected_pat = re.compile((
        "^The following group will be created:\n"
        "cn:          test_group_1\n"
        "description: Test Group One\n"
        "Do you want to continue\\? \\[y/N\\]: y\n"
        "Add user to LDAP... Done\n"
        "Add group to LDAP... Done\n"
        "Add sudo role to LDAP... Done\n"
        "Create home directory... Done\n"
        "Transaction successfully completed.\n"
        "cn:          test_group_1\n"
        "description: Test Group One\n"
        "gid_number:  \\d{5}\n$"
    ), re.MULTILINE)
    assert result.exit_code == 0
    assert expected_pat.match(result.output) is not None

    runner = CliRunner()
    result = runner.invoke(cli, ['groups', 'get', 'test_group_1'])
    expected_pat = re.compile((
        "^cn:          test_group_1\n"
        "description: Test Group One\n"
        "gid_number:  \\d{5}\n$"
    ), re.MULTILINE)
    assert result.exit_code == 0
    assert expected_pat.match(result.output) is not None

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'addmember', 'test_group_1', ldap_user.uid,
    ], input='y\n')
    expected = (
        f"Are you sure you want to add {ldap_user.uid} to test_group_1? [y/N]: y\n"
        "Add user to group... Done\n"
        "Add user to auxiliary groups... Skipped\n"
        "Subscribe user to auxiliary mailing lists... Skipped\n"
        "Transaction successfully completed.\n"
        f"Added {ldap_user.uid} to test_group_1\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, ['groups', 'get', 'test_group_1'])
    expected_pat = re.compile(f"members:\\s+{ldap_user.cn} \\({ldap_user.uid}\\)")
    assert result.exit_code == 0
    assert expected_pat.search(result.output) is not None

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'removemember', 'test_group_1', ldap_user.uid,
    ], input='y\n')
    expected = (
        f"Are you sure you want to remove {ldap_user.uid} from test_group_1? [y/N]: y\n"
        "Remove user from group... Done\n"
        "Remove user from auxiliary groups... Skipped\n"
        "Unsubscribe user from auxiliary mailing lists... Skipped\n"
        "Transaction successfully completed.\n"
        f"Removed {ldap_user.uid} from test_group_1\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, ['groups', 'delete', 'test_group_1'], input='y\n')
    assert result.exit_code == 0


def create_group(group_name, desc):
    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'add', group_name, '-d', desc,
    ], input='y\n')
    assert result.exit_code == 0


def delete_group(group_name):
    runner = CliRunner()
    result = runner.invoke(cli, ['groups', 'delete', group_name], input='y\n')
    assert result.exit_code == 0


def test_groups_multiple_members(cli_setup, new_user_gen):
    runner = CliRunner()
    create_group('test_group_1', 'Test Group 1')
    with new_user_gen() as user1, new_user_gen() as user2:
        result = runner.invoke(cli, [
            'groups', 'addmember', 'test_group_1', user1.uid, user2.uid
        ], input='y\n')
        assert result.exit_code == 0
        lines = result.output.splitlines()
        assert f'Added {user1.uid} to test_group_1' in lines
        assert f'Added {user2.uid} to test_group_1' in lines

        result = runner.invoke(cli, [
            'groups', 'removemember', 'test_group_1', user1.uid, user2.uid
        ], input='y\n')
        assert result.exit_code == 0
        lines = result.output.splitlines()
        assert f'Removed {user1.uid} from test_group_1' in lines
        assert f'Removed {user2.uid} from test_group_1' in lines
    delete_group('test_group_1')


def test_groups_with_auxiliary_groups_and_mailing_lists(cli_setup, ldap_user):
    runner = CliRunner()
    # make sure auxiliary groups + mailing lists exist in ceod_test_local.ini
    create_group('adm', 'Administrators')
    create_group('staff', 'Staff')

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'addmember', 'syscom', ldap_user.uid,
    ], input='y\n')
    expected = (
        f"Are you sure you want to add {ldap_user.uid} to syscom? [y/N]: y\n"
        "Add user to group... Done\n"
        "Add user to auxiliary groups... Done\n"
        "Subscribe user to auxiliary mailing lists... Done\n"
        "Transaction successfully completed.\n"
        f"Added {ldap_user.uid} to syscom, adm, staff\n"
        f"Subscribed {ldap_user.uid} to syscom@csclub.internal, syscom-alerts@csclub.internal\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'removemember', 'syscom', ldap_user.uid,
    ], input='y\n')
    expected = (
        f"Are you sure you want to remove {ldap_user.uid} from syscom? [y/N]: y\n"
        "Remove user from group... Done\n"
        "Remove user from auxiliary groups... Done\n"
        "Unsubscribe user from auxiliary mailing lists... Done\n"
        "Transaction successfully completed.\n"
        f"Removed {ldap_user.uid} from syscom, adm, staff\n"
        f"Unsubscribed {ldap_user.uid} from syscom@csclub.internal, syscom-alerts@csclub.internal\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'addmember', 'syscom', ldap_user.uid, '--no-subscribe',
    ], input='y\n')
    assert result.exit_code == 0
    assert 'Subscribed' not in result.output

    runner = CliRunner()
    result = runner.invoke(cli, [
        'groups', 'removemember', 'syscom', ldap_user.uid, '--no-unsubscribe',
    ], input='y\n')
    assert result.exit_code == 0
    assert 'Unsubscribed' not in result.output

    delete_group('adm')
    delete_group('staff')
