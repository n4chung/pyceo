import traceback

from zope import component

from ..AbstractTransaction import AbstractTransaction
from ceo_common.errors import UserNotSubscribedError
from ceo_common.interfaces import ILDAPService, IConfig
from ceo_common.logger_factory import logger_factory

logger = logger_factory(__name__)


class DeleteMemberTransaction(AbstractTransaction):
    """
    Transaction to permanently delete a member and their resources.
    This should only be used during development.
    """

    operations = [
        'remove_user_from_ldap',
        'remove_group_from_ldap',
        'remove_user_from_kerberos',
        'delete_home_dir',
        'unsubscribe_from_mailing_list',
    ]

    def __init__(self, uid: str):
        super().__init__()
        self.uid = uid

    def child_execute_iter(self):
        ldap_srv = component.getUtility(ILDAPService)
        cfg = component.getUtility(IConfig)
        user = ldap_srv.get_user(self.uid)
        group = ldap_srv.get_group(self.uid)
        new_member_list = cfg.get('mailman3_new_member_list')

        user.remove_from_ldap()
        yield 'remove_user_from_ldap'
        group.remove_from_ldap()
        yield 'remove_group_from_ldap'
        user.remove_from_kerberos()
        yield 'remove_user_from_kerberos'
        user.delete_home_dir()
        yield 'delete_home_dir'
        try:
            user.unsubscribe_from_mailing_list(new_member_list)
            yield 'unsubscribe_from_mailing_list'
        except UserNotSubscribedError:
            pass
        except Exception:
            logger.error(traceback.format_exc())
            yield 'failed_to_unsubscribe_from_mailing_list'

        self.finish('OK')
