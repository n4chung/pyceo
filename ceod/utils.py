import base64
import os


def gen_password() -> str:
    """Generate a temporary password."""
    return base64.b64encode(os.urandom(18)).decode()
