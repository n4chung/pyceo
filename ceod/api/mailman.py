from flask import Blueprint
from zope import component

from .utils import authz_restrict_to_staff
from ceo_common.errors import UserAlreadySubscribedError, UserNotSubscribedError, \
    NoSuchListError
from ceo_common.interfaces import IMailmanService

bp = Blueprint('mailman', __name__)


@bp.route('/<mailing_list>/<username>', methods=['POST'])
@authz_restrict_to_staff
def subscribe(mailing_list, username):
    mailman_srv = component.getUtility(IMailmanService)
    try:
        mailman_srv.subscribe(username, mailing_list)
    except UserAlreadySubscribedError as err:
        return {'error': str(err)}, 409
    except NoSuchListError as err:
        return {'error': str(err)}, 404
    return {'result': 'OK'}


@bp.route('/<mailing_list>/<username>', methods=['DELETE'])
@authz_restrict_to_staff
def unsubscribe(mailing_list, username):
    mailman_srv = component.getUtility(IMailmanService)
    try:
        mailman_srv.unsubscribe(username, mailing_list)
    except (UserNotSubscribedError, NoSuchListError) as err:
        return {'error': str(err)}, 404
    return {'result': 'OK'}
