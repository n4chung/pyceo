#!/bin/bash

set -ex

. .drone/common.sh

# set FQDN in /etc/hosts
add_fqdn_to_hosts $(get_ip_addr $(hostname)) coffee
add_fqdn_to_hosts $(get_ip_addr auth1) auth1

apt install --no-install-recommends -y default-mysql-server postgresql

# MYSQL
service mysql stop
sed -E -i 's/^(bind-address[[:space:]]+= 127.0.0.1)$/#\1/' /etc/mysql/mariadb.conf.d/50-server.cnf
service mysql start
cat <<EOF | mysql
CREATE USER IF NOT EXISTS 'mysql' IDENTIFIED BY 'mysql';
GRANT ALL PRIVILEGES ON *.* TO 'mysql' WITH GRANT OPTION;
EOF

# POSTGRESQL
service postgresql stop
POSTGRES_DIR=/etc/postgresql/11/main
cat <<EOF > $POSTGRES_DIR/pg_hba.conf
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                peer
host    all             postgres        localhost               md5
host    all             postgres        0.0.0.0/0               md5
host    all             postgres        ::/0                    md5

local   all             all                                     peer
host    all             all             localhost               md5

local   sameuser        all                                     peer
host    sameuser        all             0.0.0.0/0               md5
host    sameuser        all             ::/0                    md5
EOF
grep -Eq "^listen_addresses = '*'$" $POSTGRES_DIR/postgresql.conf || \
     echo "listen_addresses = '*'" >> $POSTGRES_DIR/postgresql.conf
service postgresql start
su -c "
cat <<EOF | psql
ALTER USER postgres WITH PASSWORD 'postgres';
REVOKE ALL ON SCHEMA public FROM public;
GRANT ALL ON SCHEMA public TO postgres;
EOF" postgres

if [ -z "$CI" ]; then
    auth_setup coffee
fi

# sync with phosphoric-acid
nc -l 0.0.0.0 9000 &
