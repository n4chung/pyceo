import click

from ..utils import http_post
from .utils import handle_sync_response


@click.group(short_help='Manage websites hosted by the main CSC web server')
def webhosting():
    pass


@webhosting.command(short_help='Disable club sites with no active club reps')
@click.option('--dry-run', is_flag=True, default=False)
@click.option('--remove-inactive-club-reps', is_flag=True, default=False)
def disableclubsites(dry_run, remove_inactive_club_reps):
    params = {}
    if dry_run:
        params['dry_run'] = 'true'
    if remove_inactive_club_reps:
        params['remove_inactive_club_reps'] = 'true'
    if not dry_run:
        click.confirm('Are you sure you want to disable the websites of clubs with no active club reps?', abort=True)

    resp = http_post('/api/webhosting/disableclubsites', params=params)
    disabled_club_names = handle_sync_response(resp)
    if len(disabled_club_names) == 0:
        if dry_run:
            click.echo('No websites would have been disabled.')
        else:
            click.echo('No websites were disabled.')
    else:
        if dry_run:
            click.echo('The following club websites would have been disabled:')
        else:
            click.echo('The following club websites were disabled:')
        for club_name in disabled_club_names:
            click.echo(club_name)
