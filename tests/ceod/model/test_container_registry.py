import pytest

from ceo_common.errors import UserNotFoundError


def test_registry(mock_harbor_server, registry_srv):
    mock_harbor_server.reset()
    username = 'test1'

    with pytest.raises(UserNotFoundError):
        registry_srv.create_project_for_user(username)

    mock_harbor_server.users.append(username)
    registry_srv.create_project_for_user(username)
    assert username in mock_harbor_server.projects
    # trying to create a project with the same name should have no effect
    registry_srv.create_project_for_user(username)

    assert registry_srv.get_accounts() == [username]

    registry_srv.delete_project_for_user(username)
    assert username not in mock_harbor_server.projects
    # trying to delete a nonexistent project should have no effect
    registry_srv.delete_project_for_user(username)
