from ...utils import http_post
from .Controller import Controller
from .SyncRequestController import SyncRequestController
import ceo.krb_check as krb
from ceo.tui.views import ResetPasswordUsePasswdView, ResetPasswordConfirmationView, ResetPasswordResponseView


class ResetPasswordController(SyncRequestController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def get_resp(self):
        return http_post(f'/api/members/{self.model.username}/pwreset')

    def get_response_view(self):
        return ResetPasswordResponseView(self.model, self, self.app)

    def on_next_button_pressed(self, button):
        try:
            self.model.username = self.get_username_from_view()
        except Controller.InvalidInput:
            return
        if self.model.username == krb.get_username():
            view = ResetPasswordUsePasswdView(self.model, self, self.app)
        else:
            view = ResetPasswordConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)
