from flask import Blueprint, request
from flask.json import jsonify
from zope import component

from .utils import authz_restrict_to_syscom, is_truthy
from ceo_common.interfaces import IUWLDAPService, ILDAPService

bp = Blueprint('uwldap', __name__)


@bp.route('/<username>')
def get_user(username: str):
    uwldap_srv = component.getUtility(IUWLDAPService)
    record = uwldap_srv.get_user(username)
    if record is None:
        return {
            'error': 'user not found',
        }, 404
    return record.to_dict()


@bp.route('/updateprograms', methods=['POST'])
@authz_restrict_to_syscom
def update_programs():
    ldap_srv = component.getUtility(ILDAPService)
    if request.headers.get('content-type') == 'application/json':
        body = request.get_json()
    else:
        body = {}
    kwargs = {'members': body.get('members')}
    if body.get('dry_run') or is_truthy(request.args.get('dry_run', 'false')):
        kwargs['dry_run'] = True
    return jsonify(
        ldap_srv.update_programs(**kwargs)
    )
