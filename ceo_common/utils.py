import datetime


def get_current_datetime() -> datetime.datetime:
    # We place this in a separate function so that we can mock it out
    # in our unit tests.
    return datetime.datetime.now()
