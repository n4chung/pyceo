# TODO: fix Drone
chmod 1777 /tmp

# don't resolve container names to *real* CSC machines
sed -E 's/([[:alnum:]-]+\.)*uwaterloo\.ca//g' /etc/resolv.conf > /tmp/resolv.conf
# remove empty 'search' lines, if we created them
sed -E -i '/^search[[:space:]]*$/d' /tmp/resolv.conf
# also remove the 'rotate' option, since this can cause the Docker DNS server
# to be circumvented
sed -E -i '/^options.*\brotate/d' /tmp/resolv.conf
cp /tmp/resolv.conf /etc/resolv.conf
rm /tmp/resolv.conf

# normally systemd creates /run/ceod for us
mkdir -p /run/ceod

# mock out systemctl
ln -sf /bin/true /usr/local/bin/systemctl
# mock out acme.sh
mkdir -p /root/.acme.sh
ln -sf /bin/true /root/.acme.sh/acme.sh
# mock out kubectl
cp .drone/mock_kubectl /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl
# add k8s authority certificate
mkdir -p /etc/csc
cp .drone/k8s-authority.crt /etc/csc/k8s-authority.crt
# openssl is actually already present in the python Docker image,
# so we don't need to mock it out

# netcat is used for synchronization between the containers
export DEBIAN_FRONTEND=noninteractive
apt update
apt install -y netcat-openbsd
if [ "$(hostname)" != auth1 ]; then
    # ceod uses Augeas, which is not installed by default in the Python
    # Docker container
    apt install -y libaugeas0
fi

get_ip_addr() {
    getent hosts $1 | cut -d' ' -f1
}

add_fqdn_to_hosts() {
    ip_addr=$1
    hostname=$2
    sed -E "/${ip_addr}.*\\b${hostname}\\b/d" /etc/hosts > /tmp/hosts
    cp /tmp/hosts /etc/hosts
    rm /tmp/hosts
    echo "$ip_addr $hostname.csclub.internal $hostname" >> /etc/hosts
}

sync_with() {
    host=$1
    port=9000
    if [ $# -eq 2 ]; then
        port=$2
    fi
    synced=false
    # give it 20 minutes (can be slow if you're using e.g. NFS or Ceph)
    for i in {1..240}; do
        if nc -vz $host $port ; then
            synced=true
            break
        fi
        sleep 5
    done
    test $synced = true
}

auth_setup() {
    hostname=$1

    # LDAP
    apt install -y --no-install-recommends libnss-ldapd
    service nslcd stop || true
    cp .drone/ldap.conf /etc/ldap/ldap.conf
    grep -Eq '^map group member uniqueMember$' /etc/nslcd.conf || \
        echo 'map group member uniqueMember' >> /etc/nslcd.conf
    sed -E -i 's/^uri .*$/uri ldap:\/\/auth1.csclub.internal/' /etc/nslcd.conf
    sed -E -i 's/^base .*$/base dc=csclub,dc=internal/' /etc/nslcd.conf
    cp .drone/nsswitch.conf /etc/nsswitch.conf

    # KERBEROS
    apt install -y krb5-user libpam-krb5 libsasl2-modules-gssapi-mit
    cp .drone/krb5.conf /etc/krb5.conf

    if [ $hostname = phosphoric-acid ]; then
        sync_port=9000
    elif [ $hostname = coffee ]; then
        sync_port=9001
    else
        sync_port=9002
    fi
    sync_with auth1 $sync_port

    rm -f /etc/krb5.keytab
    cat <<EOF | kadmin -p sysadmin/admin -w krb5
addprinc -randkey host/$hostname.csclub.internal
ktadd host/$hostname.csclub.internal
addprinc -randkey ceod/$hostname.csclub.internal
ktadd ceod/$hostname.csclub.internal
EOF
    if [ $hostname = phosphoric-acid ]; then
        cat <<EOF | kadmin -p sysadmin/admin -w krb5
addprinc -randkey ceod/admin
ktadd ceod/admin
EOF
    fi
    service nslcd start
}
