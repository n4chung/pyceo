from flask import Blueprint, request
from zope import component

from .utils import authz_restrict_to_syscom, create_streaming_response
from ceo_common.interfaces import ILDAPService, IConfig
from ceod.transactions.members import UpdateMemberPositionsTransaction

bp = Blueprint('positions', __name__)


@bp.route('/', methods=['GET'], strict_slashes=False)
def get_positions():
    ldap_srv = component.getUtility(ILDAPService)

    positions = {}
    for user in ldap_srv.get_users_with_positions():
        for position in user.positions:
            positions[position] = user.uid

    return positions


@bp.route('/', methods=['POST'], strict_slashes=False)
@authz_restrict_to_syscom
def update_positions():
    cfg = component.getUtility(IConfig)
    body = request.get_json(force=True)

    required = cfg.get('positions_required')
    available = cfg.get('positions_available')

    # remove falsy values
    body = {
        positions: username for positions, username in body.items()
        if username
    }

    for position in body.keys():
        if position not in available:
            return {
                'error': f'unknown position: {position}'
            }, 400

    for position in required:
        if position not in body:
            return {
                'error': f'missing required position: {position}'
            }, 400

    txn = UpdateMemberPositionsTransaction(body)
    return create_streaming_response(txn)
