import click
from zope import component

from ceo_common.interfaces import IConfig

from ..utils import http_post, http_put, http_get, http_delete
from .utils import Abort, handle_sync_response, print_colon_kv


@click.group(short_help='Perform operations on the CSC cloud')
def cloud():
    pass


@cloud.group(short_help='Manage your cloud account')
def account():
    pass


@account.command(short_help='Activate your cloud account')
def activate():
    cfg = component.getUtility(IConfig)
    base_domain = cfg.get('base_domain')

    resp = http_post('/api/cloud/accounts/create')
    handle_sync_response(resp)
    lines = [
        'Congratulations! Your cloud account has been activated.',
        f'You may now login into https://cloud.{base_domain} with your CSC credentials.',
        "Make sure to enter 'Members' for the domain (no quotes).",
    ]
    for line in lines:
        click.echo(line)


@cloud.group(short_help='Manage cloud accounts')
def accounts():
    pass


@accounts.command(short_help='Purge expired cloud accounts')
def purge():
    resp = http_post('/api/cloud/accounts/purge')
    result = handle_sync_response(resp)
    click.echo('Accounts to be deleted: ' + ','.join(result['accounts_to_be_deleted']))
    click.echo('Accounts which were deleted: ' + ','.join(result['accounts_deleted']))


@cloud.group(short_help='Manage your virtual hosts')
def vhosts():
    pass


@vhosts.command(name='add', short_help='Add a virtual host')
@click.argument('domain')
@click.argument('ip_address')
def add_vhost(domain, ip_address):
    body = {'ip_address': ip_address}
    if '/' in domain:
        raise Abort('invalid domain name')
    click.echo('Please wait, this may take a while...')
    resp = http_put('/api/cloud/vhosts/' + domain, json=body)
    handle_sync_response(resp)
    click.echo('Done.')


@vhosts.command(name='delete', short_help='Delete a virtual host')
@click.argument('domain')
def delete_vhost(domain):
    if '/' in domain:
        raise Abort('invalid domain name')
    resp = http_delete('/api/cloud/vhosts/' + domain)
    handle_sync_response(resp)
    click.echo('Done.')


@vhosts.command(name='list', short_help='List virtual hosts')
def list_vhosts():
    resp = http_get('/api/cloud/vhosts')
    result = handle_sync_response(resp)
    vhosts = result['vhosts']
    if not vhosts:
        click.echo('No vhosts found.')
        return
    pairs = [(d['domain'], d['ip_address']) for d in vhosts]
    print_colon_kv(pairs)
